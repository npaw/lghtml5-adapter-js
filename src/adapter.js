var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.LgHtml5 = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.currentTime
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.duration
  },

  /** Override to return resource URL. */
  getResource: function () {
    return this.player.currentSrc
  },

  getIsLive: function () {
    return this.player.duration === Infinity
  },


  getPlayrate: function() {
    return this.player.playbackRate
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'LG_WEB_OS'
  },

  /** Register listeners to this.player. */
  registerListeners: function () {
    // Console all events if logLevel=DEBUG
    // youbora.Util.logAllEvents(this.player)

    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true, 1200)

    // Register listeners
    this.references = {
      play: this.playListener.bind(this),
      pause: this.pauseListener.bind(this),
      playing: this.playingListener.bind(this),
      error: this.errorListener.bind(this),
      seeking: this.seekingListener.bind(this),
      seeked: this.playingListener.bind(this),
      ended: this.endedListener.bind(this),
      timeupdate: this.timeupdateListener.bind(this),
      //waiting: this.bufferingListener.bind(this)
    }

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      delete this.references
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    this.fireStart()
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  timeupdateListener: function (e) {
    if ((this.getPlayhead() > 1 || (this.plugin && this.plugin.getIsLive())) && !this.player.error) {
      this.fireStart()
      this.fireJoin()
    }
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    if (!this.flags.isJoined) {
      this.lastDuration = this.getDuration()
    }
    this.fireJoin()
    if (this.flags.isBuffering && this.monitor && typeof this.monitor.canBeUsed === 'function' && !this.monitor.canBeUsed()){
      this.fireBufferEnd()
    }
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    var msg = null
    var code = null
    try {
      if (e && e.target && e.target.error) {
        code = e.target.error.code
        msg = e.target.error.message
      }
    } catch (err) {
      // nothing
    }
    this.fireFatalError(code, msg)
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    this.fireSeekBegin()
  },

  /** Listener for 'ended' event. */
  endedListener: function (e) {
    this.fireStop({ playhead: this.lastDuration })
  },

  bufferingListener: function(e) {
    if (this.monitor && typeof this.monitor.canBeUsed === 'function' && !this.monitor.canBeUsed()){
      this.fireBufferBegin()
    }
  }
})

module.exports = youbora.adapters.LgHtml5
