## [6.8.2] - 2022-05-31
### Improved
- Improve buffer detection.
### Library
- Packaged with `lib 6.8.56`.

## [6.8.1] - 2022-05-31
### Added
- Improved jointime detection
### Library
- Packaged with `lib 6.8.21`

## [6.8.0] - 2021-11-29
### Added
- Playrate detection
- Improved seek/buffer detection for live content
### Library
- Packaged with `lib 6.8.9`

## [6.7.2] - 2020-02-02
### Added
- Error descriptions
- Live detection
### Library
- Packaged with `lib 6.7.26`

## [6.7.1] - 2020-05-29
### Library
- Packaged with `lib 6.7.7`

## [6.7.0] - 2020-05-14
### Removed
- Built without .map file
### Library
- Packaged with `lib 6.7.6`

## [6.5.3] - 2020-01-08
### Library
- Packaged with `lib 6.5.23`
### Added
- Monitor interval increased from 800 to 1200

## [6.5.2] - 2019-10-23
### Library
- Packaged with `lib 6.5.18`

## [6.5.1] - 2019-07-02
### Fixed
- Monitor check

## [6.5.0] - 2019-06-27
### Added
- Seek monitor
### Library
- Packaged with `lib 6.5.5`

## [6.4.0] - 2018-08-17
### Library
- Packaged with `lib 6.4.1`

## [6.1.0] - 2017-10-25
### Library
- Packaged with `lib 6.1.0`

## [6.0.0] - 2017-07-26
### Library
- Packaged with `lib 6.0.4`
